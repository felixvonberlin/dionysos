/**
dionysos
Copyright (C) 2023-2025 Felix v. Oertzen
dionysos@hyrrokkin.eu

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/

var drunkenDrinks = 0;

function loadPersonalValues() {
  var weight = localStorage.getItem("weight");
  var body_size = localStorage.getItem("body_size");
  var sex = localStorage.getItem("sex");

  if (weight && body_size && sex) {
    document.getElementById("in_weight").value = weight;
    document.getElementById("in_body_size").value = body_size;
    document.getElementById("in_sex").value = sex;
  } 
}

function savePersonalValues() {
  localStorage.weight = document.getElementById("in_weight").value;
  localStorage.body_size = document.getElementById("in_body_size").value;
  localStorage.sex = document.getElementById("in_sex").value;
}

function addDrink() {
  drunkenDrinks++;
  exp = document.getElementById("cont_drinks");
  
  newDrink  = '<span class="drunken_drink_name">drink ' + drunkenDrinks + '</span>'
            + '<input type="number" id="in_volalc_' + drunkenDrinks +'" onchange="polling()" oninput="polling()" required name="volalc_' + drunkenDrinks +'" value="5" min="1" max="99" step="0.5">&#37;'
            + '<input type="number" id="in_volwat_' + drunkenDrinks +'" onchange="polling()" oninput="polling()"  required name="volwat_' + drunkenDrinks +'" value="0.5" min="1" step="0.5">'
            + '<select name="amount" id="in_amount_' + drunkenDrinks +'" onchange="polling()" oninput="polling()" >'
              + '<option value="l">l</option>'
              + '<option value="cl">cl</option>'
            + '</select><br>';
  exp.insertAdjacentHTML('beforeend', newDrink);
  determineDrunkness ();
}

function determineDrunkness () {
  alc_mililiters = 0;

  for (var i = 1; i <= drunkenDrinks; i++) {
    drinks_promille = Number(document.getElementById("in_volalc_" + i).value)
    drinks_liquid   = Number(document.getElementById("in_volwat_" + i).value)
    drinks_measure  = Number((()=>{
        switch (document.getElementById("in_amount_" + i).value) {
            case "cl":
                return 10
            case "l":
                return 1000
            default:
                return 1;
        }
    })());
    alc_mililiters = alc_mililiters + (drinks_promille/100) * (drinks_liquid * drinks_measure);
  }
  hum_weight = Number(document.getElementById("in_weight").value) // weight of the human who consumes
  hum_size   = Number(document.getElementById("in_body_size").value) // body size of the human who consumes
  t_consumpt = Number(document.getElementById("in_duration").value)
  hum_reduct = Number((()=>{
        switch (document.getElementById("in_sex").value) {
            case "female":
                return 0.31223 - 0.006446 * hum_weight + 0.004466 * hum_size;
            case "male":
                return 0.31608 - 0.004821 * hum_weight + 0.004432 * hum_size;
            case "children":
                return 0.85
        }
    })()); 

  alc_weight = alc_mililiters * 0.8; // consumed alc in gramms
  // Widmark formula
  blood_alcohol_level = alc_weight / (hum_weight * hum_reduct)
  blood_alcohol_level = blood_alcohol_level * (1 - 0.15); //Resorptionsdefizit
  blood_alcohol_level = blood_alcohol_level - (t_consumpt*0.15)
  
  document.getElementById("h1_promille_display").innerHTML = Number(Math.max(0, blood_alcohol_level)).toFixed(2) + " &#8240;";
  getSymptoms(Number(Math.max(0, blood_alcohol_level)).toFixed(2));
}

function getSymptoms(bal) {
  var symptoms = "";
  if (bal < 0.1) {} else
  if (bal < 0.3) { symptoms = "<li>slowed reactions</li><li>lowered inhibitions</li>" } else
  if (bal < 0.6) { symptoms = "<li>feeling of warmth</li><li>loss of inhibition<li>mild eiphoria</li>" } else
  if (bal < 1.0) { symptoms = "<li>slurred speech</li><li>impaired judgement</li><li>lowered balance</li>"} else
  if (bal < 2.0) { symptoms = "<li>loss of coordination</li><li>impaired motor skills</li><li>possible nausea</li>" } else
  if (bal < 3.0) { symptoms = "<li>numb to pain</li><li>impaired gag reflex</li><li>blackouts</li>"} else
  if (bal < 4.0) { symptoms = "<li>difficulties staying awake</li><li>lowered hearth rate</li><li>lowered body temperature</li>" } else
  if (bal < 5.0) { symptoms = "<li>onset of comas</li><li>Bodily functions as breathing may stop</li>" } else
                  {symptoms = "<li>preserved in ethanol for eternity</li><li>also known as death</li>"}
  document.getElementById("h1_promille_description").innerHTML = "<ul>" + symptoms + "</ul>";

}

function snackbar(snacktype) {
  var x = document.getElementById("snackbar_" + snacktype);
  x.className = x.className + " show";
  setTimeout(function(){ x.className = x.className.replace("show", ""); }, 6000);
}

loadPersonalValues();
alert("This page is meant for educational purposes only. The information generated from it is not intended to replace the medical advice of a doctor and should not be relied upon; nor do the information generated from the page constitute legal advice. Please consult your healthcare provider for advice about a specific medical condition and legal counsel for any legal questions. This page just gives a rough approximation because of the numerous factors and complexities relating to alcohol consumption by of different individuals. In addition to the gender, body weight and amount of alcohol consumed in a time period, blood alcohol content of any individual person is influenced by that person’s metabolism, health issues, medications taken, history of alcohol consumption and the amount of food and non-alcoholic beverages eaten before or during alcohol consumption, among other factors.");
document.addEventListener("click", function (e) {determineDrunkness()});

function polling() {
  determineDrunkness();
  savePersonalValues();
}

let input_fields = document.getElementsByClassName("input_field");
for (let i = 0; i < input_fields.length; i++) {
  input_fields[i].addEventListener('change', polling);
  input_fields[i].addEventListener('input', polling);
}

polling();
