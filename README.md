# Dionysos
Dionysos is a simple BAC (blood alcohol calculator) which does BAC-typical things.
Its named by Dionysus, a god of wine-making in the Greek mythology.


I developed it because multiple pages in the internet seem a kind of untrustworthy.

The calculation is based on a German [web page](https://www.smart-rechner.de/promille/rechner.php#_1 "Promillerechner auf smart-rechner.de") which explains the situation quite well.

You can use this page [here](https://hyrrokkin.eu/dionysos).